import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { StudentService } from 'src/app/student.service';
import { Router } from '@angular/router';
import { Student } from 'src/app/students';


@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.scss'],

})




export class ClassroomComponent implements OnInit {

  public selectedRow: number [] = [];
  @HostListener('document:click', ['$event.target']) clicked2() {
    this.selectedRow
   }


  rows = new UntypedFormControl('');
  numberOfRows = new UntypedFormControl();
  numberOfColumns = new UntypedFormControl();



  maxValueRow : number = 0;

  public student : Student = {id: 0, JMBAG: '', name: '', surname: ''};

  constructor(
    public studentService: StudentService,
    private router: Router,) { }

  ngOnInit(): void {
    this.studentService.matrix
   }



  updateRows() {

    if(this.rows.value.length != 0){
      this.maxValueRow = this.studentService.numberOfRows
      let s = this.rows.value.split(',');

      var numbers:number[] = [];

        s.forEach(function(number : any){
          if(!number.includes('-'))
          {
            numbers.push(parseInt(number))
          }
          else{
            let range=number.split('-');
            let i : number;
            for(i=parseInt(range[0]);i<=parseInt(range[1]); i++ )
                {
                numbers.push(i)
                }
          }

        });


      numbers = [...new Set(numbers)];
      numbers.sort((a,b)=>a-b)

      for(let i = 0; i<numbers.length; i++)
      {
        if(numbers[i]>this.maxValueRow)
        {
          numbers.splice(i)
          i=numbers.length
        }
      }

      console.log(numbers)
      this.rows.setValue(numbers.toString())
      this.selectedRow = numbers
       }
  }

  makeMatrix(){

    var i = 0
    var j =0

    for(i=0; i<this.numberOfRows.value; i++){
    this.studentService.matrix[i]=[];
      for(j=0;j<this.numberOfColumns.value;j++){
        this.studentService.matrix[i][j] = this.student
        }
    }

    this.studentService.setMatrixDimensions(this.numberOfRows.value, this.numberOfColumns.value)

    console.log(this.studentService.matrix)
  }

  selectRow(e: Event, number:number){
    var num ;
    num = number+1
    if(this.selectedRow.includes(num)){
      for(let i=0; i<this.selectedRow.length; i++){
        if(this.selectedRow[i]==num)
        this.selectedRow.splice(i,1)
        if(this.selectedRow.length == 0){
          this.rows.setValue('')
        }
        this.rows.setValue(this.selectedRow.toString())
      }
    }else{
      this.selectedRow.push(num)
      this.rows.setValue(this.selectedRow.toString())
    }
  }


  public goToPage(pageName:string){
    this.router.navigate([`${pageName}`]);
  }



}
