import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{
  path:'classroom',
  loadChildren: () => import('./classroom/classroom.module').then(m => m.ClassroomModule)
},
{
 path:'studentList',
 loadChildren: () => import('./students/students.module').then(m => m.StudentsModule)
},
{
  path: '**', redirectTo: 'classroom', pathMatch: 'full'
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
