import { Student } from './students';

export const ACCOUNTS: Student[] = [
      { id: 11, JMBAG:"125478961111", name: "Marko", surname: 'Lozic' },
      { id: 12, JMBAG:"125478962222", name: "Luka", surname: 'Franjic' },
      { id: 13, JMBAG:"125487413333", name: "Josko", surname: 'Maric' },
      { id: 14, JMBAG:"125478934444", name: "Josip", surname: 'Petrovic' },
      { id: 15, JMBAG:"125478965555", name: "Franjo", surname: 'Lozic' },

];
