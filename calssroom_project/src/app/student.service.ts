import { Inject, inject, Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Student } from './students';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})



export class StudentService {


  private studentUrl = 'api/students';

  public numberOfRows: number = 0;
  public numberOfCols: number = 0;

  public matrix : Student [][] = [[]];

  constructor(
    private http : HttpClient,
  ) { }

  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>(this.studentUrl)
  }

  deleteStudent(id: number): Observable<Student> {
    const url = `${this.studentUrl}/${id}`;
    return this.http.delete<Student>(url)
  }

  updateStudent(student:Student): Observable<Student> {
    const url = `${this.studentUrl}/${student.id}`;
    return this.http.put<Student>(url, student)

  }

  addNew(student:Student): Observable<Student> {
    const url = `${this.studentUrl}/${student.id}`;
    return this.http.post<Student>(url, student)

  }

  setMatrixDimensions(numberOfRows : number, numberOfCols : number): void{
    this.numberOfRows = numberOfRows;
    this.numberOfCols = numberOfCols
  }

}
