import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Student } from './students';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const students = [
      { id: 11, JMBAG:"125478961111", name: "Marko", surname: 'Lozic'},
      { id: 12, JMBAG:"125478962222", name: "Luka", surname: 'Franjic'},
      { id: 13, JMBAG:"125487413333", name: "Josko", surname: 'Maric'},
      { id: 14, JMBAG:"125478934444", name: "Josip", surname: 'Petrovic'},
      { id: 15, JMBAG:"125478965555", name: "Franjo", surname: 'Lozic'},
    ];
    return {students};
  }
  genId(students: Student[]): number {
    return students.length > 0 ? Math.max(...students.map(student => student.id)) + 1 : 11;
  }
}
