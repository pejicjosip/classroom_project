import { Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { EMPTY, empty, Observable } from 'rxjs';
import { StudentService } from 'src/app/student.service';
import { Student } from 'src/app/students';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss'],

})

export class StudentListComponent implements OnInit {

  public students: Student[] = [];
  students$!: Observable<Student[]>;
  public selectedStudent: Student = {id: 0, JMBAG: '', name: '', surname: ''};
  public student : Student = {id: 0, JMBAG: '', name: '', surname: ''};


  studentName = new UntypedFormControl('');
  studentSurname = new UntypedFormControl('');
  studentJMBAG = new UntypedFormControl('');
  studentID = new UntypedFormControl();
  rowToAssign = new UntypedFormControl();
  public rowPrint : number = 0;
  numberOfRows : number = 0;
  numberOfCols : number = 0;

  studentFound : boolean = false;
  rowFull : boolean = false;
  studentAdded : boolean = false;


  public addStudentDisplayStatus= false;

  constructor(
    public StudentService : StudentService,
    private router: Router,

  ) { }

  ngOnInit(): void {
    this.getStudent();
    this.numberOfRows = this.StudentService.numberOfRows;
    this.numberOfCols = this.StudentService.numberOfCols

  }

  public getStudent(){
    this.StudentService.getStudents().subscribe(data =>{
    this.students = data;
    },error => {console.log(error)});
  }

  public goToPage(pageName:string){
    this.router.navigate([`${pageName}`]);
  }
  public selectStudent(e: Event, student:Student) {
    this.selectedStudent = student;
    e.stopPropagation();

    this.studentName.setValue(this.selectedStudent.name)
    this.studentSurname.setValue(this.selectedStudent.surname)
    this.studentJMBAG.setValue(this.selectedStudent.JMBAG)
    this.studentID.setValue(this.selectedStudent.id)
    this.rowFull = false
    this.studentFound = false
    this.studentAdded = false
  }

  deleteStudent(): void {

    if(this.selectedStudent.id !== 0) {
      this.StudentService.deleteStudent(this.selectedStudent.id).subscribe(data => {
        this.getStudent();
      }, error => {
      });
    }
    else{
      console.log("no selcted acc")
    }

    this.studentName.setValue('')
    this.studentSurname.setValue('')
    this.studentJMBAG.setValue('')
    this.studentID.setValue('')

  }

  updateStudentData(){
    this.selectedStudent.name = this.studentName.value
    this.selectedStudent.surname = this.studentSurname.value
    this.selectedStudent.JMBAG = this.studentJMBAG.value
    this.StudentService.updateStudent(this.selectedStudent).subscribe(data => {this.getStudent();},error => {
    });

  }

  public showModal(){
      this.addStudentDisplayStatus = !this.addStudentDisplayStatus;
  }

  public stopPropagation(e: Event): void {
    e.stopPropagation();
  }

  closeModalFunc(data: any){
    this.addStudentDisplayStatus=data;
    this.getStudent();

  }

  assignStudent(){

    for( let i=0; i<this.numberOfRows; i++){
      for(let j=0; j<this.numberOfCols; j++){
        if(JSON.stringify(this.StudentService.matrix[i][j]) === JSON.stringify(this.selectedStudent)){
          this.studentFound = true;
        }
      }
    }

    if(this.studentFound == false){
      for(let j=0; j<this.numberOfCols; j++){
        if(JSON.stringify(this.StudentService.matrix[this.rowToAssign.value - 1][j]) === JSON.stringify(this.student)){
          this.StudentService.matrix[this.rowToAssign.value -1][j] = this.selectedStudent
          this.rowPrint = this.rowToAssign.value
          j = this.numberOfCols
          this.studentAdded = true;
        }

      }
      if(this.studentAdded == false){
        this.rowFull = true;
      }
    }

  }

}
