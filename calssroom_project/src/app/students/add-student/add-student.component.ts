import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { StudentService } from 'src/app/student.service';
import { Student } from 'src/app/students';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {

  @Output() closeModalFunc:EventEmitter<any>=new EventEmitter()

  studentName = new UntypedFormControl('');
  studentSurname = new UntypedFormControl('');
  studentJMBAG = new UntypedFormControl('');
  studentID = new UntypedFormControl('');

  public toAddStudent: Student = {id: 0, JMBAG: '', name: '', surname: ''};
  public students: Student[] = [];



  constructor(
    private StudentService : StudentService,
  ) { }

  ngOnInit(): void {
  }

  closeModal(){
    let data=false;
    this.closeModalFunc.emit(data)
  }

  addStudent(){
    this.toAddStudent.name = this.studentName.value
    this.toAddStudent.surname = this.studentSurname.value
    this.toAddStudent.JMBAG = this.studentJMBAG.value
    this.toAddStudent.id = parseInt(this.studentID.value)
    this.StudentService.addNew(this.toAddStudent)
      .subscribe(student => {
        this.students.push(student);
      });
  }



}
